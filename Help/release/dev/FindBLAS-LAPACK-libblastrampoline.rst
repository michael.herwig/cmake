FindBLAS-LAPACK-libblastrampoline
---------------------------------

* The :module:`FindBLAS` and :module:`FindLAPACK` modules gained
  support for ``libblastrampoline``.
